from neo4j import GraphDatabase
import os
import subprocess
import requests
import json
import sys
import urllib
from getpass import getpass
import time
from datetime import datetime
import threading
task = []
def create_folder(path):
    try:
        os.mkdir(path)
    except Exception:
        pass
class Neo4j():
    def __init__(self):
        self.url = 'bolt://localhost:7687'
        self.user = 'neo4j'
        self.password = 'Ynhutren123'
        self.driver = GraphDatabase.driver(self.url, auth=(self.user, self.password))
    def close(self):
        self.driver.close()
    def exec(self, command):
        with self.driver.session() as session:
            result = list(session.run(command))
            return result
    def getAnime(self):
        with self.driver.session() as session:
            result = list(session.run('match (anime: Anime) return anime'))
            _list = []
            for record in result:
                _dict = {'title': record.get('anime').get('title'), 'url': record.get('anime').get('url')}
                _list.append(_dict)
            return _list
    def getChapter(self, url):
        with self.driver.session() as sesstion:
            result = list(sesstion.run('match (anime: Anime { url: $url})-[:IS_CHAPTER]->(chapter) return chapter', url=url))
            # return result\
            _list = []
            for record in result:
                _dict = {'number': record.get('chapter').get('number'), 'url': record.get('chapter').get('url')}
                _list.append(_dict)
            return _list
    def downloadLink(self, url):
        with self.driver.session() as session:
            try:
                result = list(session.run('match (chapter: Chapter { url: $url})-[:LINK_DOWN]->(download) return download', url=url))
                # return result[0]
                return result[0].get('download').get('url')
            except:
                return False


def download_video(path, url):
    subprocess.run(['ffmpeg', '-i', url, '-c', 'copy', '-bsf:a', 'aac_adtstoasc','-vcodec', 'copy', path])




def upload(local, remote):
    subprocess.run(['rclone', 'copy' ,local , 'onedrive:' + remote])

def run():
    if len(task) <= 0: 
        return
    _task = task.pop()
    basic_path = _task[0]
    title = _task[1]
    chapter = _task[2]
    url = _task[3]
    path = basic_path + '/' + title + '/' + chapter + '.mp4'
    remote = basic_path + '/' + title
    create_folder(remote)
    download_video(path, url)
    upload(path, remote)
    os.remove(path)

def main():

    base_folder = 'video'
    create_folder(base_folder)
    neo = Neo4j()
    allAnime = neo.getAnime()
    for anime in allAnime:
        title = anime['title']
        # create_folder(base_folder + '/' + title)
        # path = base_folder + '/' + title
        chapters = neo.getChapter(anime['url'])
        for chapter in chapters:
            path = base_folder + '/' + title + '/' + chapter['number'] + '.mp4'
            url = neo.downloadLink(chapter['url'])
            if url == False:
                continue
            # download_video(path=path, url=url)
            task.append([base_folder, title, chapter['number'], url])
    
    while len(task) > 0:
        thread_list = []
        for i in range(0,8):
            thread_list.append(threading.Thread(target=run))
        for i in thread_list:
            i.start()
        for i in thread_list:
            i.join()
            # print(task[0][0])
    # subprocess.run(['ffmpeg', '-i', 'https://tc-1.dayimage.net/_v6/ce1b209551bd8aa26c48e23eb977c9dffd0e9b4a7356e56cc852fc67eed39afee5ba8680d9dc34294fbd254b38af43bbee3428abc569dd0f3bc7bc02bdc2fca6b1ccd9c7e6f65deab9b52c9302dc2b7d93cc0a64eca87b24f989abe0e2198d50948e6d2f94471e539e200ebbd18f29d9/master.m3u8', '-c', 'copy', '-bsf:a', 'aac_adtstoasc', 'video/output.mp4'])

    



if __name__ == '__main__':
    main()

