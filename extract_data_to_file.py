from neo4j import GraphDatabase
import sys
import os

class Neo4j():
    def __init__(self):
        self.url = 'bolt://localhost:7687'
        self.user = 'neo4j'
        self.password = 'Ynhutren123'
        self.driver = GraphDatabase.driver(self.url, auth=(self.user, self.password))
    def close(self):
        self.driver.close()
    def exec(self, command):
        with self.driver.session() as session:
            result = list(session.run(command))
            return result
    def getAnime(self):
        with self.driver.session() as session:
            result = list(session.run('match (anime: Anime) return anime'))
            _list = []
            for record in result:
                _dict = {'title': record.get('anime').get('title'), 'url': record.get('anime').get('url')}
                _list.append(_dict)
            return _list
    def getChapter(self, url):
        with self.driver.session() as sesstion:
            result = list(sesstion.run('match (anime: Anime { url: $url})-[:IS_CHAPTER]->(chapter) return chapter', url=url))
            # return result\
            _list = []
            for record in result:
                _dict = {'number': record.get('chapter').get('number'), 'url': record.get('chapter').get('url')}
                _list.append(_dict)
            return _list
    def downloadLink(self, url):
        with self.driver.session() as session:
            try:
                result = list(session.run('match (chapter: Chapter { url: $url})-[:LINK_DOWN]->(download) return download', url=url))
                # return result[0]
                return result[0].get('download').get('url')
            except:
                return False


def main():
    db = Neo4j()
    animes = db.getAnime()
    for anime in animes:
        chapters = db.getChapter(anime['url'])
        for chapter in chapters:
            download = db.downloadLink(chapter['url'])
            if download == False:
                continue
            with open('data.txt', 'a') as file:
                file.write(anime['title'] + '|' + chapter['number'] + '|' + download)
                file.write('\n')




if __name__ == '__main__':
    main()