import subprocess
import os
task = []
def create_folder(path):
    try:
        os.mkdir(path)
    except Exception:
        pass
def main():
    lines = []
    base_folder = 'video'

    with open('data.txt', 'r') as file:
        for line in file:
            lines.append(line.split('\n')[0])

    for line in lines:
        title, number, url = line.split('|')
        task.append([base_folder, title, number, url])
    while len(task) > 0:
        run()

def upload(local, remote):
    subprocess.run(['rclone', 'copy' ,local , 'onedrive:' + remote])
def run():
    if len(task) <= 0: 
        return
    _task = task.pop()
    basic_path = _task[0]
    title = _task[1]
    chapter = _task[2]
    url = _task[3]
    path = basic_path + '/' + title + '/' + chapter + '.mp4'
    remote = basic_path + '/' + title
    create_folder(basic_path)
    create_folder(remote)
    download_video(path, url)
    upload(path, remote)
    os.remove(path)
def download_video(path, url):
    subprocess.run(['ffmpeg', '-i', url, '-c', 'copy', '-bsf:a', 'aac_adtstoasc','-vcodec', 'copy', path])
if __name__ == '__main__':
    main()